import {BaseApi} from '../../../shared/core/base-api';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Category} from '../models/category.model';
import {Observable} from 'rxjs';

@Injectable()
export class CategoryService extends BaseApi {

  constructor(http: HttpClient) {
    super(http);
  }

  public addCategory(category: Category): Observable<Category> {
    return this.post('categories', category);
  }
}
