import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export abstract class BaseApi {

  private static readonly baseUrl = 'http://localhost:3000/';

  protected constructor(
    protected http: HttpClient
  ) {}

  private static addBaseUrl(url: string = ''): string {
    return BaseApi.baseUrl + url;
  }

  protected get(url: string = ''): Observable<any> {
    return this.http.get(BaseApi.addBaseUrl(url));
  }

  protected post(url: string = '', data: any = {}): Observable<any> {
    return this.http.post(BaseApi.addBaseUrl(url), data);
  }

  protected put(url: string = '', data: any = {}): Observable<any> {
    return this.http.put(BaseApi.addBaseUrl(url), data);
  }

}
