export class AuthService {

  private _isAuthenticated = false;

  login() {
    this._isAuthenticated = true;
  }

  logout() {
    this._isAuthenticated = false;
    window.localStorage.clear();
  }

  get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }
}
